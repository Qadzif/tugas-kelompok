[![pipeline](https://gitlab.com/Qadzif/tugas-kelompok/badges/master/pipeline.svg)](https://trash-it.herokuapp.com/)
# Tugas Kelompok PPW : TRASH-IT

Link heroku : [https://trash-it.herokuapp.com/](https://trash-it.herokuapp.com/)

Anggota:
1. Iqrar Agalosi Nureyza
2. Naufal Thirafy Prakoso
3. Muhammad Marandi Mllendila Efendi
4. Qadzif Kamil Zahari


## TRASH-IT
Adalah sebuah aplikasi yang bertujuan untuk meningkatkan kepedulian masyarakat akan pentingnya membuang sampah.
Aplikasi ini memiliki mekanisme dimana pengguna yang membuang sampah akan diberi upah. Caranya adalah 
dengan menginput lokasi rumah, berat sampah, jenis sampah. Maka nanti karyawan dari TPA (Tempat Pembuangan Akhir) akan menjemput sampah
tersebut dalam kurun waktu tertentu dan pengguna akan mendapatkann reward di akun mereka sesuai dengan nominal berat dan jenis sampah mereka.
Dengan begitu masyarakat akan memiliki semangat dan kepedulian terhadap sampah.


### Fitur yang diimplementasikan:
- Fitur **Buang Sampah**

Digunakan untuk submit sampah yang akan dibuang dengan mengisi form, yaitu **Lokasi Pembuangan Sampah**, **Berat Sampah**, **Kategori Sampah**

- Fitur **Status**

Digunakan untuk menampilkan status dari sampah yang sudah disubmit, statusnya antara lain: **Submitted**, **Accepted**

- Fitur **Riwayat**

Digunakan untuk menampilkan riwayat transaksi ketika suatu transaksi sudah berstatus **Accepted**

- Fitur **FAQ**

Menampilan Frequently Asked Question perihal aplikasi *TRASH-IT*

- Fitur **Tutorial**

Menampilkan Tutorial penggunaan aplikasi *TRASH-IT*

- Fitur **Profil**

Menampilkan profil user dan beberapa informasi pribadi seperti: *Pekerjaan*, *Lokasi*, *Dompetku*