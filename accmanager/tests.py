from django.test import TestCase
from django.test import Client
from .models import Dummy, Account
from .views import snippet_to_account

_client = Client()

# Create your tests here.

class AccManagerUnitTest(TestCase):

    def test_account_add(self):
        Account.objects.create(name='custom', job='work', address='location')
        self.assertEqual(len(Account.objects.all()), 1)

    def test_default_profile_redirect(self):
        response = _client.get('/profile/')
        self.assertIn(response.status_code, (200, 301, 302))

    def test_default_profile_with_existing_account(self):
        Account.objects.create(name='custom', job='work', address='location')
        response = _client.get('/profile/')
        self.assertIn(response.status_code, (200, 301, 302))

    def test_profile_with_existing_id(self):
        Account.objects.create(name='custom', job='work', address='location')
        response = _client.get('/profile/1')
        self.assertIn(response.status_code, (200, 301, 302))

    def test_profile_using_template(self):
        Account.objects.create(name='custom', job='work', address='location')
        response = _client.get('/profile/1')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_with_not_existing_id(self):
        response = _client.get('/profile/1')
        self.assertIn(response.status_code, (200, 301, 302))
    
    def test_riwayat_default_url(self):
        response = _client.get('/profile/riwayat')
        self.assertEqual(response.status_code, 200)
    
    def test_status_default_url(self):
        response = _client.get('/profile/status')
        self.assertEqual(response.status_code, 200)
    
    def test_edit_url(self):
        response = _client.get('/profile/edit')
        self.assertIn(response.status_code, (200, 301, 302))
    
    def test_snippet_to_account(self):
        dummy_list = []
        for i in range(3):
            dummy = Dummy()
            dummy.berat_Sampah = '1'
            dummy.lokasi_Rumah = '1'
            dummy.kategori_Sampah = ['1', '2', '3']
            dummy_list.append(dummy)
        result = snippet_to_account(dummy_list)
        self.assertEqual(len(dummy_list), 3)

    pass
