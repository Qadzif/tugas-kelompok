from django.db import models

# Create your models here.

class Dummy(object):
    pass

class Account(models.Model):
    name = models.CharField(max_length=10485760, default='')
    job = models.CharField(max_length=10485760, default='')
    address = models.CharField(max_length=10485760, default='')
    balance = models.IntegerField(default=0)

class Riwayat(models.Model):
    owner_id = models.ForeignKey('Account', on_delete=models.CASCADE, null=True)
    jenis = models.CharField(max_length=10485760, default='')
    berat = models.IntegerField()
    lokasi = models.CharField(max_length=10485760, default='')
    diterima = models.BooleanField(default=False)
    pass