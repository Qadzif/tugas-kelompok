from django.shortcuts import render
from .models import Dummy, Account, Riwayat
from buangsampah.models import Snippet
from django.http import HttpResponseRedirect
from django.urls import reverse

def snippet_to_account(snippet_query):
    _return = []
    for i in snippet_query:
        dummy = Dummy()
        dummy.berat = i.berat_Sampah
        dummy.lokasi = i.lokasi_Rumah
        dummy.jenis = '|'.join(i.kategori_Sampah)
        _return.append(dummy)
    return _return


# Create your views here.

def profile(request, id=None):
    link_riwayat = reverse('accmgr:riwayat')
    link_status = reverse('accmgr:status')
    link_edit = reverse('accmgr:edit')

    filtered = Account.objects.filter(id=id)
    if len(filtered) > 0:
        selaccount = filtered.first()

        selbalance = str(selaccount.balance)
        balance = '.'.join([selbalance[::-1][i:i+3] for i in range(0, len(selbalance), 3)])[::-1]
        balance = 'Rp ' + balance + ',00'
        link_riwayat = reverse('accmgr:riwayat-id', args=[selaccount.id])
        link_status = reverse('accmgr:status-id', args=[selaccount.id])
        link_edit = reverse('accmgr:edit-id', args=[selaccount.id])
    elif len(Account.objects.all()) > 0:
        selaccount = Account.objects.first()
        return HttpResponseRedirect(reverse('accmgr:profile-id', args=[selaccount.id]))
    elif id != None:
        return HttpResponseRedirect(reverse('accmgr:profile'))
    else:
        selaccount = Dummy()
        selaccount.name = 'Dummy Account'
        selaccount.job = 'Students'
        selaccount.address = 'depok'
        selaccount.id = None
        balance = 'Rp 18.000,00'

    return render(request, 'profile.html', context={
        'selaccount' : selaccount,
        'balance' : balance,
        'link_riwayat' : link_riwayat,
        'link_status' : link_status,
        'link_edit' : link_edit,
    })

def edit(request, id=None):
    return HttpResponseRedirect(reverse('accmgr:profile'))

def riwayat(request, id=None):
    # list_riwayat = Riwayat.objects.filter(owner_id=id, diterima=True)
    list_riwayat = snippet_to_account(Snippet.objects.filter(status_Diterima=True))
    return render(request, 'riwayat.html', context={
        'list_riwayat' : list_riwayat,
        'owner' : id if id != None else 1,
    })

def status(request, id=None):
    # list_riwayat = Riwayat.objects.filter(owner_id=id, diterima=False)
    list_riwayat = snippet_to_account(Snippet.objects.filter(status_Diterima=False))
    return render(request, 'status.html', context={
        'list_riwayat' : list_riwayat,
        'owner' : id if id != None else 1,
    })