from django.urls import path

from . import views

app_name = 'accmgr'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('<int:id>', views.profile, name='profile-id'),
    path('edit', views.edit, name='edit'),
    path('edit/<int:id>', views.edit, name='edit-id'),
    path('riwayat', views.riwayat, name='riwayat'),
    path('riwayat/<int:id>', views.riwayat, name='riwayat-id'),
    path('status', views.status, name='status'),
    path('status/<int:id>', views.status, name='status-id'),
]