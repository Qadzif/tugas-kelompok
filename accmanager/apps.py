from django.apps import AppConfig


class AccmanagerConfig(AppConfig):
    name = 'accmanager'
