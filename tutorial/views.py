from django.shortcuts import render, redirect

# Create your views here.
def tutorialViews(request):
    return render(request, 'tutorial.html')