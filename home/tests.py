from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import homeViews
from django.http import HttpRequest
from unittest import skip


# Create your tests here.

class HomeTest(TestCase):

    def test_home_url_is_exist(self):
        response = Client().get(reverse('home'))
        self.assertEqual(response.status_code, 200)


