from django.shortcuts import render, redirect

# Create your views here.
def homeViews(request):
    return render(request, 'home.html')