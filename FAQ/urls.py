from django.urls import path
from . import views

app_name = 'FAQ'

urlpatterns = [
    path('', views.faqViews, name='faq'),
]
