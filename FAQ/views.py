from django.shortcuts import render, redirect

# Create your views here.
def faqViews(request):
    return render(request, 'FAQ.html')