from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('FAQ/', views.faq, name='faq'),
    path('tutorial/', views.tutorial, name='tutorial'),
    path('profile/', include('accmanager.urls')),
    # path('', include('helloworld.urls')),
    path('buangsampah/', include('buangsampah.urls')),
]
