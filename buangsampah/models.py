from django.db import models
from multiselectfield import MultiSelectField

class Snippet(models.Model):
    TRASH_CHOICES = (
        ('Plastik', 'Plastik'),
        ('Kertas', 'Kertas'), 
        ('Sisa Makanan', 'Sisa Makanan'),
        ('Logam', 'Logam'),
        ('Kaca', 'Kaca'),
        ('Lain-lain', 'Lain-lain')
    )
    
    berat_Sampah = models.CharField(max_length = 100)
    lokasi_Rumah = models.TextField()
    kategori_Sampah = MultiSelectField(choices = TRASH_CHOICES)
    status_Diterima = models.BooleanField(default=False)

    def __str__(self):
        return self.lokasi_Rumah
        return self.kategori_Sampah
        return self.berat_Sampah
