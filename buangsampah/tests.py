from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.conf import settings
from .views import *
from .models import *

class buangsampah_test(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_template_exist(self):
        response = Client().get('/buangsampah/')
        self.assertTemplateUsed(response, 'form.html')

    def test_using_index_func(self):
        found = resolve('/buangsampah/')
        self.assertEqual(found.func, snippet_detail)

    